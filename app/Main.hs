module Main (main) where

import qualified Starburst as S
import qualified Waves as W

-- run the examples one after the other
main :: IO ()
main = S.main >> W.main
