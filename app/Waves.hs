module Waves where

import Control.Monad.Reader
import Data.Colour
import Data.Colour.Names
import Hpix
import Numeric.Noise.Ridged
import System.Random (getStdRandom, randomR)

-- width, height, half-width, and half-height
w, h, hw, hh :: Float
(w, h, hw, hh) = (500, 500, w / 2, h / 2)

main :: IO ()
main = do
  seed <- getStdRandom $ randomR (1, 10000000)
  window "Waves" w h (waves seed)

-- This displays a cool ocean-like graphic, with the 'waves' moving based on noise.
waves :: Int -> RenderLoop
waves s t = do
  clearTo black
  mapM_ pix' vals
    where vals = [(realToFrac x, (realToFrac y) + (realToFrac $ 5 * noise s x y t))
                 | x <- [0..w] , y <- [0,15..h]]
          -- `pix'` is just `pix` but with color blending
          pix' loc@(x, y) = pix loc $ blend (y / h) navy royalblue    

-- Returns a 3-dimensional ridged noise generator given a seed
noise :: (Real a) => Int -> a -> a -> a -> Double
noise seed x y z = noiseValue gen (x', y', z')
  where gen = ridged seed 5 0.05 1 2
        -- Make sure the coordinates get converted to doubles.
        -- `noise` could just accept only Doubles, but it's better
        -- to be flexible here for convenience.
        x' = realToFrac x :: Double
        y' = realToFrac y :: Double
        z' = realToFrac z :: Double
